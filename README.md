# Emacs Configuration

## Installation

Just have to clone the repo in your emacs directory (`emacs.default` for me) with sub modules.
``` git 
git clone --recurse-submodules https://gitlab.isima.fr/arbarraux/emacs-config.git
```

Then you will have to put this only line in your `init.el` file:
``` emacs-lisp
(org-babel-load-file "~/.emacs.default/config.org")
```
Obviously, you need to change the path to your config file.
Now the configuration should be great and you can make any changs you want on the org file.

## Key bindings
| Keybind | Action                                  |
|:--------|:----------------------------------------|
| `ù`     | Insert the dereference arrow in C `->`. |
| `C-c r` | Reload the config file (*init.el*)      |
| `C-c c` | Open the org config file.               |
| `C-x é` | Split window vertically.                |
| `C-x "` | Split window horizontaly.               |
| `C-x à` | Close current window.                   |
|         |                                         |
